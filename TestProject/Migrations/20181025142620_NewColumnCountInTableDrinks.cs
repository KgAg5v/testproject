﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestProject.Migrations
{
    public partial class NewColumnCountInTableDrinks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Count",
                table: "Drinks",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count",
                table: "Drinks");
        }
    }
}
