﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TestProject.Database.Entities;

namespace TestProject.Database
{
    public class VendingMachineContext : IdentityDbContext
    {
        public const string ADMIN_ROLE_NAME = "ADMIN";

        public DbSet<Coin> Coins { get; set; }
        public DbSet<Drink> Drinks { get; set; }

        public VendingMachineContext(DbContextOptions<VendingMachineContext> options) : base(options)
        { }
    }
}
