﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.Database.Entities
{
    /// <summary>
    /// Напиток
    /// </summary>
    public class Drink : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Название напитка
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        [Required]
        public int Cost { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        [Required]
        public int Count { get; set; }
    }
}
