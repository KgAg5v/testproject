﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.Database.Entities
{
    /// <summary>
    /// Общий для всех сущностей бд интерфейс
    /// </summary>
    public interface IEntity
    {
        int Id { get; set; }
    }
}
