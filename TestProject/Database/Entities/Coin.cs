﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestProject.Database.Entities
{
    /// <summary>
    /// Значения монет
    /// </summary>
    public enum CoinValue
    {
        One = 1,
        Two = 2,
        Five = 5,
        Ten = 10
    }

    /// <summary>
    /// Монетка
    /// </summary>
    public class Coin : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        /// <summary>
        /// Номинал
        /// </summary>
        [Required]        
        public CoinValue Value { get; set; }

        /// <summary>
        /// Количество в автомате
        /// </summary>
        [Required]
        public int Count { get; set; }

        /// <summary>
        /// Признак блокировки
        /// </summary>
        [Required]
        public bool IsBlocked { get; set; }
    }
}
