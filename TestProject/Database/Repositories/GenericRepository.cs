﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Database.Entities;

namespace TestProject.Database.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class, IEntity
    {
        protected VendingMachineContext DbContext { get; set; }

        public GenericRepository(VendingMachineContext dbContext)
        {
            DbContext = dbContext;
        }

        public virtual async Task Create(T element)
        {
            await DbContext.Set<T>().AddAsync(element);
            await DbContext.SaveChangesAsync();
        }

        public virtual async Task Delete(T element)
        {
            DbContext.Set<T>().Remove(element);
            await DbContext.SaveChangesAsync();
        }

        public virtual async Task<T> Get(int id)
        {
            return await DbContext.Set<T>().FindAsync(id);
        }

        public virtual async Task Update(T element)
        {
            DbContext.Set<T>().Update(element);
            await DbContext.SaveChangesAsync();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return DbContext.Set<T>();
        }
    }
}
