﻿using TestProject.Database.Entities;

namespace TestProject.Database.Repositories.CoinsRepository
{
    public interface ICoinsRepository : IGenericRepository<Coin>
    {
    }
}
