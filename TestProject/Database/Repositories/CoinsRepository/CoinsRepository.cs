﻿using TestProject.Database.Entities;

namespace TestProject.Database.Repositories.CoinsRepository
{
    public class CoinsRepository : GenericRepository<Coin>, ICoinsRepository
    {
        public CoinsRepository(VendingMachineContext dbContext) : base(dbContext)
        {
        }
    }
}
