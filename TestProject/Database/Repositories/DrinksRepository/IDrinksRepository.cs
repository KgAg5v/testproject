﻿using TestProject.Database.Entities;

namespace TestProject.Database.Repositories.DrinksRepository
{
    public interface IDrinksRepository : IGenericRepository<Drink>
    {
    }
}
