﻿using TestProject.Database.Entities;

namespace TestProject.Database.Repositories.DrinksRepository
{
    public class DrinksRepository : GenericRepository<Drink>, IDrinksRepository
    {
        public DrinksRepository(VendingMachineContext dbContext) : base(dbContext)
        {
        }
    }
}
