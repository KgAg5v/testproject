﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Database.Entities;

namespace TestProject.Database.Repositories
{
    public interface IGenericRepository<T> where T : class, IEntity
    {
        Task Update(T element);
        IEnumerable<T> GetAll();
        Task Delete(T element);
        Task<T> Get(int id);
        Task Create(T element);

    }
}
