﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.Database
{
    public static class SeedData
    {       
        public static async Task Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new VendingMachineContext(
                serviceProvider.GetRequiredService<DbContextOptions<VendingMachineContext>>()))
            {                

                var adminID = await EnsureUser(serviceProvider, "123", "admin@test.com");
                await EnsureRole(serviceProvider, adminID, VendingMachineContext.ADMIN_ROLE_NAME);

                SeedDB(context);
            }
        }

        private static async Task<string> EnsureUser(IServiceProvider serviceProvider,
                                                    string testUserPw, string UserName)
        {
            var userManager = serviceProvider.GetService<UserManager<IdentityUser>>();

            var user = await userManager.FindByNameAsync(UserName);
            if (user == null)
            {
                user = new IdentityUser { UserName = UserName };
                await userManager.CreateAsync(user, testUserPw);                
            }

            return user.Id;
        }

        private static async Task<IdentityResult> EnsureRole(IServiceProvider serviceProvider,
                                                                      string uid, string role)
        {
            IdentityResult IR = null;
            var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();

            if (!await roleManager.RoleExistsAsync(role))
            {
                IR = await roleManager.CreateAsync(new IdentityRole(role));
            }

            var userManager = serviceProvider.GetService<UserManager<IdentityUser>>();

            var user = await userManager.FindByIdAsync(uid);

            IR = await userManager.AddToRoleAsync(user, role);

            return IR;
        }
        
        
        public static void SeedDB(VendingMachineContext context)
        {
            if (context.Coins.Any())
            {
                return;   
            }

            context.Coins.Add(new Entities.Coin { Count = 0, IsBlocked = false, Value = Entities.CoinValue.One });
            context.Coins.Add(new Entities.Coin { Count = 0, IsBlocked = false, Value = Entities.CoinValue.Two });
            context.Coins.Add(new Entities.Coin { Count = 0, IsBlocked = false, Value = Entities.CoinValue.Five });
            context.Coins.Add(new Entities.Coin { Count = 0, IsBlocked = false, Value = Entities.CoinValue.Ten });

            context.SaveChanges();
        }
    }
}
