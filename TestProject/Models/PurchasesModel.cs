﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.Database;
using TestProject.Database.Entities;
using TestProject.Database.Repositories.CoinsRepository;
using TestProject.Database.Repositories.DrinksRepository;

namespace TestProject.Models
{
    public class PurchaseException : Exception
    {
        public PurchaseException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Модель для покупки
    /// </summary>
    public class PurchasesModel
    {
        private readonly ICoinsRepository _coins;
        private readonly IDrinksRepository _drinks;
        private readonly VendingMachineContext _context;

        public PurchasesModel(VendingMachineContext context)
        {
            _coins = new CoinsRepository(context);
            _drinks = new DrinksRepository(context);
            _context = context;
        }

        public async Task<IEnumerable<Coin>> Purchase(int drinkId, Coin[] coinsToPurchase)
        {
            var drink = await _drinks.Get(drinkId);

            if (drink == null || drink.Count == 0)
            {
                throw new PurchaseException("Не верно выбран напиток");
            }

            var dbCoins = _coins.GetAll().ToList();
            //Если есть хотя бы один, не удовлетворяющий условию
            if (!coinsToPurchase.All(
                    coin =>
                        dbCoins.Any(
                            //Все монеты должны иметь положительное количество (включая ноль) и не быть заблокированными
                            dbCoin => coin.Count == 0 || (coin.Count > 0 && !dbCoin.IsBlocked)
                            )
                    )
                )
            {
                throw new PurchaseException("Не верно выбраны монеты к оплате");
            }

            //Получаем сумму сдачи
            var change = coinsToPurchase.Where(coin => coin.Count > 0).Sum(coin => (int)coin.Value * coin.Count) - drink.Cost;

            //Напиток стоит дороже, чем пришедшая сумма
            if (change < 0)
            {
                throw new PurchaseException("Цена напитка превышает сумму монет");
            }

            //Получаем монеты для сдачи
            var coinsToChange = dbCoins.Where(dbCoin => dbCoin.IsBlocked && dbCoin.Count > 0);

            var remainder = GetRemainderCoins(change, coinsToChange);

            if (remainder == null)
            {
                throw new PurchaseException("Не достаточно сдачи");
            }

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {


                    drink.Count--;
                    await _drinks.Update(drink);

                    //Кладем в базу новые монеты
                    foreach (var coin in coinsToPurchase.Where(coin => coin.Count > 0))
                    {
                        var dbCoin = dbCoins.First(x => x.Value == coin.Value);
                        dbCoin.Count += coin.Count;
                        await _coins.Update(dbCoin);
                    }

                    //Пересчитываем в базе монеты в сдаче
                    foreach (var coin in coinsToChange)
                    {
                        await _coins.Update(coin);
                    }

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw new PurchaseException("Ошибка при покупке. Попробуйте снова.");
                }
            }

            return remainder;
        }

        /// <summary>
        /// Получает сдачу и изменяет количество монет
        /// </summary>
        /// <param name="change">необходимая сдача</param>
        /// <param name="availableCoins">доступные монеты, с которых будет списанно необходимое количество</param>
        /// <returns>Сдача</returns>
        private IEnumerable<Coin> GetRemainderCoins(int change, IEnumerable<Coin> availableCoins)
        {
            List<Coin> remainder = new List<Coin>();
            
            foreach (var coin in availableCoins.OrderByDescending(x => (int)x.Value))
            {
                if (change > 0)
                {
                    var countNeed = change / (int)coin.Value;

                    if (countNeed == 0)
                    {
                        continue;
                    } 
                    
                    var remainderCount = coin.Count - countNeed >= 0 ? coin.Count - countNeed : 0;

                    var remainderCoin = new Coin
                    {
                        Count = coin.Count - remainderCount,
                        Value = coin.Value,
                        Id = coin.Id,
                        IsBlocked = coin.IsBlocked
                    };

                    coin.Count = remainderCount;

                    remainder.Add(remainderCoin);

                    change -= (int)coin.Value * remainderCoin.Count;
                }
                else
                {
                    break;
                }
            }

            if (change > 0)
            {
                return null;
            }

            return remainder;
        }
    }
}
