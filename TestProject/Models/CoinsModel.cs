﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Database;
using TestProject.Database.Entities;
using TestProject.Database.Repositories.CoinsRepository;

namespace TestProject.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CoinsModel
    {
        private readonly ICoinsRepository _repository;

        public CoinsModel(VendingMachineContext context)
        {
            _repository = new CoinsRepository(context);
        }

        public IEnumerable<Coin> GetAllCoins()
        {
            return _repository.GetAll();
        }

        public async Task<Coin> GetCoin(int id)
        {
            return await _repository.Get(id);
        }
        
        public async Task<bool> UpdateCoin(Coin coin)
        {
            try
            {
                await _repository.Update(coin);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (coin != null && await _repository.Get(coin.Id) == null)
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
            return true;
        }
    }
}
