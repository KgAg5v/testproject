﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Database;
using TestProject.Database.Entities;
using TestProject.Database.Repositories.DrinksRepository;

namespace TestProject.Models
{
    /// <summary>
    /// Модель для работы с напитками
    /// </summary>
    public class DrinksModel
    {
        private readonly IDrinksRepository _repository;

        public DrinksModel(VendingMachineContext context)
        {
            _repository = new DrinksRepository(context);
        }

        public IEnumerable<Drink> GetAllDrinks()
        {
            return _repository.GetAll();
        }

        public async Task<Drink> GetDrink(int id)
        {
            return await _repository.Get(id);
        }

        public async Task AddDrink(Drink drink)
        {
            await _repository.Create(drink);
        }

        public async Task<bool> UpdateDrink(Drink drink)
        {
            try
            {
                await _repository.Update(drink);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (drink != null && await _repository.Get(drink.Id) == null)
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
            return true;
        }

        public async Task<bool> DeleteDrink(int id)
        {
            var element = await _repository.Get(id);

            if (element == null)
            {
                return false;
            }

            await _repository.Delete(element);
            return true;
        }
        
    }
}
