﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestProject.Database;
using TestProject.Database.Entities;
using TestProject.Models;

namespace TestProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchasesController : Controller
    {
        private readonly VendingMachineContext _context;

        public PurchasesController(VendingMachineContext context)
        {
            _context = context;
        }

        // POST: api/Purchases/5
        [HttpPost("{id}")]
        public async Task<IActionResult> Purchase([FromRoute] int id, [FromBody] Coin[] coinsToPurchase)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                return Ok(await new PurchasesModel(_context).Purchase(id, coinsToPurchase));
            }
            catch (PurchaseException pe)
            {
                return BadRequest(pe.Message);
            }
            catch (Exception e)
            {
                throw;
            }            
        }
    }
}