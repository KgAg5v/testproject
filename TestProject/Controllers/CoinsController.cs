﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestProject.Database;
using TestProject.Database.Entities;
using TestProject.Models;

namespace TestProject.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CoinsController : Controller
    {
        private readonly VendingMachineContext _context;

        public CoinsController(VendingMachineContext context)
        {
            _context = context;
        }

        // GET: api/Coins
        [HttpGet]
        [AllowAnonymous]
        public IEnumerable<Coin> GetCoins()
        {
            return new CoinsModel(_context).GetAllCoins();
        }

        // GET: api/Coins/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCoin([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }                       

            var coin = await new CoinsModel(_context).GetCoin(id);

            if (coin == null)
            {
                return NotFound();
            }

            return Ok(coin);
        }

        // PUT: api/Coins/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCoin([FromRoute] int id, [FromBody] Coin coin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != coin.Id)
            {
                return BadRequest();
            }

            if (!await new CoinsModel(_context).UpdateCoin(coin))
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}