﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestProject.Database;
using TestProject.Database.Entities;
using TestProject.Models;

namespace TestProject.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DrinksController : Controller
    {
        private readonly VendingMachineContext _context;

        public DrinksController(VendingMachineContext context)
        {
            _context = context;
        }

        // GET: api/Drinks
        [AllowAnonymous]
        [HttpGet]
        public IEnumerable<Drink> GetDrinks()
        {
            return new DrinksModel(_context).GetAllDrinks();
        }

        // GET: api/Drinks/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDrink([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var drink = await new DrinksModel(_context).GetDrink(id);

            if (drink == null)
            {
                return NotFound();
            }

            return Ok(drink);
        }

        // PUT: api/Drinks/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDrink([FromRoute] int id, [FromBody] Drink drink)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != drink.Id)
            {
                return BadRequest();
            }

            if (!await new DrinksModel(_context).UpdateDrink(drink))
            {
                return NotFound();
            }          

            return NoContent();
        }

        // POST: /Drinks
        [HttpPost]
        public async Task<IActionResult> PostDrink([FromBody] Drink drink)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await new DrinksModel(_context).AddDrink(drink);

            return CreatedAtAction("GetDrink", new { id = drink.Id }, drink);
        }

        // DELETE: api/Drinks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDrink([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!await new DrinksModel(_context).DeleteDrink(id))
            {
                return NotFound();
            }

            return Ok();
        }
    }
}