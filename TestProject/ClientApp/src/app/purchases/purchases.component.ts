import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Coin } from '../coins/coins.component'
import { Drink } from '../drinks/drinks.component'
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.css'],
})
export class PurchasesComponent {
  //Массив монет для показа
  public coinList: Coin[];
  //Массив напитков
  public drinkList: Drink[];
  //Брошенные монеты
  public coinCount: { [id: number]: number; } = {};
  //Итоговая сумма
  public sum = 0;

  public urlComponent = "api/Purchases";

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, public snackBar: MatSnackBar) {
    this.http.get<Coin[]>(this.baseUrl + "api/Coins").subscribe(result => {
      this.coinList = result;
      this.fillCoinsCount();
    }, error => console.error(error));
    this.http.get<Drink[]>(this.baseUrl + "api/Drinks").subscribe(result => {
      this.drinkList = result;
    }, error => console.error(error));
  }

  public addCoin(id: number, value: number) {
    this.coinCount[id]++;
    this.sum += value;
  }

  public buyDrink(id: number) {
    //post
    var coinsToBuy = this.coinList.map<Coin>(x =>
    {
      var newCoin = new Coin();
      newCoin.id = x.id;
      newCoin.value = x.value;
      newCoin.count = this.coinCount[x.id];
      return newCoin;
    });

    this.http.post<Coin[]>(`${this.baseUrl}${this.urlComponent}/${id}`, coinsToBuy).subscribe(result => {
      var change = "";

      if (result.length > 0) {
        for (var i = 0; i < result.length; i++) {
          change += `${result[i].value} x${result[i].count}`;
        }
      }
      else {
        change = "нет";
      }

      this.snackBar.open("Сдача: ", change, {
        duration: 10000,
      });

      this.clearAll();
    }, error => { this.snackBar.open(error.error, "", { duration: 2000, }); this.clearAll(); });
  }

  private clearAll() {
    this.coinCount = {};
    this.fillCoinsCount();
    this.sum = 0;
  }

  private fillCoinsCount() {
    for (var i = 0; i < this.coinList.length; i++) {
      this.coinCount[this.coinList[i].id] = 0;
    }
  }
}
