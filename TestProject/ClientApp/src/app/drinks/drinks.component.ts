import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.component.html',
  styleUrls: ['./drinks.component.css'],
})
export class DrinksComponent {
  public drinkList: Drink[];

  public loggedIn = false;

  public urlComponent = "api/Drinks";


  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, public dialog: MatDialog, private router: Router) {
    this.reload();
  }

  public reload() {
    this.http.get<Drink[]>(this.baseUrl + this.urlComponent).subscribe(result => {
      this.drinkList = result;
    }, error => console.error(error));
  }

  public addNewDrink() {

    const dialogRef = this.dialog.open(DialogDrinks, {
      width: '250px',
      data: { id: 0, name: "", cost: 0 }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result.name != "" && result.cost != 0) {
        this.http.post(this.baseUrl + this.urlComponent, result)
          .subscribe(result => { }, error => { console.error(error); });
        this.reload();
      }
    });
  }
  
  public editDrink(id: number) {
    this.http.get<Drink>(`${this.baseUrl}${this.urlComponent}/${id}`).subscribe(result => {
      const dialogRef = this.dialog.open(DialogDrinks, {
        width: '250px',
        data: result,
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log(result);
        if (result.name != "" && result.cost != 0) {
          this.http.put(`${this.baseUrl}${this.urlComponent}/${id}`, result)
            .subscribe(result => { }, error => { console.error(error); });
          this.reload();
        }
      });
    });

  }

  public deleteDrink(id: number) {
    this.http.delete(`${this.baseUrl}${this.urlComponent}/${id}`)
      .subscribe(result => { this.reload(); }, error => { console.error(error); });
  }
}

@Component({
  selector: 'dialog-drinks',
  templateUrl: 'dialog-drinks.html',
})
export class DialogDrinks {

  constructor(
    public dialogRef: MatDialogRef<DialogDrinks>,
    @Inject(MAT_DIALOG_DATA) public data: Drink) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export class Drink {
  id: number;
  name: string;
  cost: number;
  count: number;
}
