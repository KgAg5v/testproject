import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-auth',
  templateUrl: 'auth.component.html',
})
export class AuthComponent {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {

  }

  onSubmit({ value, valid }: { value: AuthData, valid: boolean }) {

    this.http.post<string>(this.baseUrl + 'api/Auth/Login', value)
      .subscribe(result => { console.log(result) }, error => console.error(error));
  }

}

export class AuthData {
  email: string;
  password: string;
  rememberMe: boolean;
  returnUrl: string;
}
