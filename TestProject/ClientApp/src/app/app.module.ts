import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { DrinksComponent } from './drinks/drinks.component';
import { DialogDrinks } from './drinks/drinks.component';
import { CoinsComponent } from './coins/coins.component';
import { DialogCoins } from './coins/coins.component';
import { PurchasesComponent } from './purchases/purchases.component';
import { AuthComponent } from './auth/auth.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    DrinksComponent,
    DialogDrinks,
    CoinsComponent,
    DialogCoins,
    PurchasesComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatGridListModule,
    MatCardModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatSnackBarModule,
    RouterModule.forRoot([
      { path: '', component: PurchasesComponent, pathMatch: 'full' },
      { path: 'coins', component: CoinsComponent },
      { path: 'drinks', component: DrinksComponent },
      { path: 'auth', component: AuthComponent },
    ])
  ],
  providers: [],
  entryComponents: [
    DialogDrinks,
    DialogCoins
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
