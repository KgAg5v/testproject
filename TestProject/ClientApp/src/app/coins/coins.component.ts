import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-coins',
  templateUrl: './coins.component.html',
  styleUrls: ['./coins.component.css'],
})
export class CoinsComponent {
  public coinList: Coin[];
  public urlComponent = "api/Coins";

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, public dialog: MatDialog) {
    this.reload();
  }

  public reload() {
    this.http.get<Coin[]>(this.baseUrl + this.urlComponent).subscribe(result => {
      this.coinList = result;
    }, error => console.error(error));
  }
  
  public editCoin(id: number) {
    this.http.get<Coin>(`${this.baseUrl}${this.urlComponent}/${id}`).subscribe(result => {
      const dialogRef = this.dialog.open(DialogCoins, {
        width: '300px',        
        data: result,
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log(result);
        if (result.count >= 0) {
          this.http.put(`${this.baseUrl}${this.urlComponent}/${id}`, result)
            .subscribe(result => { }, error => { console.error(error); });
          this.reload();
        }
      });
    });

  }
}

@Component({
  selector: 'dialog-coins',
  templateUrl: 'dialog-coins.html',
})
export class DialogCoins {

  constructor(
    public dialogRef: MatDialogRef<DialogCoins>,
    @Inject(MAT_DIALOG_DATA) public data: Coin) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

export class Coin {
  id: number;
  value: string;
  count: number;
  isBlocked: boolean;
}
